# Projet de stage titre CDA AFPA
Ce projet a été développé avec la version 5.0 de Symfony.

# Installation
Avant tout vous devez installer symfony sur votre pc, pour ce faire allez directement sur le site : https://symfony.com/download

Une fois téléchargé et installé on va installer le projet

Pour installer le projet il faut suivre les suivantes étapes :

- cloner ce repo sur votre pc

- ouvrir le terminal ou git bash (si vous êtes sous windows) dans le dossier du repo et taper la commande suivante : 

    - `composer install` (permet d'installer toutes les dépendances utilisés dans les projet)


- une fois la BDD configurée dans le fichier .env vous devez rentrer les commandes suivantes :

    - `php bin/console doctrine:database:create` (permet de créer la BDD)

    - `php bin/console doctrine:migrations:migrate` (permet de lancer la migration à la BDD et créer les différentes tables)

    - `php bin/console doctrine:fixtures:load` (permet de lancer les fixtures et créer des fausses donées pour tester l'app)


- Pour finir, dans le terminal on va se placer dans le dossier du projet et taper la commande suivante : 

    - `symfony server:start` ou `symfony serve` (permet de lancer le serveur local et tester l'app)

# Les fonctionnalités implementés
- Login / logout
- S'enregistrer
- Créer des annonces
- Définir les equipemments du logement
- Définir le type de logement proposé
- Définir les thématiques de l'annonce
- Ajouter plusiers images dans les annonces
- Réserver une annonce
- Noter et commenter une annonce
- Suivre des utilisateurs et être suivi (follow et followers)
- Liker une annonce
- Faire une recherche en applquant des flitres.
- Affichage de l'ensemble des annonces dans une carte dynamique implemmenté avec "leafletjs" et "OpenStreetMaps".
- Possibilité d'ajouter dynamiquement l'adresse du logement lors de la création d'une annonce.
- Acceder à un back-office pour gérer les annonces, les réservations, les commentaires et les utilisateurs.
