<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $booker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ad;

    //  Si le le mets dans les annontations, ca ne marche pas @Assert\Date(message="La date d'arrive n'est pas dans le bon format !")
    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThan("today", message="La date d'arrive au logement doit être superièure à celle d'aujourd'hui", groups={"front"})
     */
    private $arriveDate;

    // Si le le mets dans les annontations, ca ne marche pas @Assert\Date(message="La date d'arrive n'est pas dans le bon format !")
    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThan(propertyPath="arriveDate", message="La date de départ doit être superièure à celle d'arrivée")
     * 
     */
    private $leaveDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="float")
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(value = 0, message="Le nombre de voyageurs doit être superieur à 0")
     * 
     */
    private $travelers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooker(): ?User
    {
        return $this->booker;
    }

    public function setBooker(?User $booker): self
    {
        $this->booker = $booker;

        return $this;
    }

    public function getAd(): ?Ad
    {
        return $this->ad;
    }

    public function setAd(?Ad $ad): self
    {
        $this->ad = $ad;

        return $this;
    }

    public function getArriveDate(): ?\DateTimeInterface
    {
        return $this->arriveDate;
    }

    public function setArriveDate(\DateTimeInterface $arriveDate): self
    {
        $this->arriveDate = $arriveDate;

        return $this;
    }

    public function getLeaveDate(): ?\DateTimeInterface
    {
        return $this->leaveDate;
    }

    public function setLeaveDate(\DateTimeInterface $leaveDate): self
    {
        $this->leaveDate = $leaveDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getTravelers(): ?int
    {
        return $this->travelers;
    }

    public function setTravelers(int $travelers): self
    {
        $this->travelers = $travelers;

        return $this;
    }

    /**
     * On enregistre la date de création de la réservation et le montantTotal
     *  @ORM\PrePersist
     *  @ORM\PreUpdate
     * 
     * @return void
     */
    public function prePersist(){
        // Si le champ date de création est vide, on lui donne la date au moment de la réservation
        if(empty($this->createdAt)) {
            $this->createdAt = new \DateTime();
        }

        // On calcule le montant total du séjour
        if(empty($this->totalPrice)) {
            $this->totalPrice = $this->ad->getPrice() * $this->getTotalDays();
        }
    }

    // On calcule le nombre de jours totals de la reservation, on calcule la difference entre la date de sortie et d'entrée
    public function getTotalDays() {
        $total_days = $this->leaveDate->diff($this->arriveDate);

        return $total_days->days;
    }

    /**
     * Permet de verifier si les dates choisies par l'utilisateur sont disponibles
     *
     * @return boolean
     */ 
    public function isBookedDates() {
        // 1) Il faut connaitre les dates qui ne sont pas disponibles pour l'annonce
        $notAvailableDays = $this->ad->getNotAvailableDays();

        // 2) On compare les dates choisies avec les dates non disponibles
        $bookingDays = $this->getBookingDays();

        $formatDay = function($day){
            return $day->format('Y-m-d');
        };

        // Tableau des chaines de caractères des jours reservés
        $days = array_map($formatDay, $bookingDays);

        $notAvailable = array_map($formatDay, $notAvailableDays);
        
        // On compare les deux tableaux, s'il y a une correspondance, les dates ne sont pas disponibles, s'il ne trouve de correspondance, les dates sont disponible
        foreach($days as $day) {
            if(array_search($day, $notAvailable) !== false) {
                return false;
            }
        }
        return true;
    }

    /**
     * Permet de récupérer un tableau avec les jours qui correspondent à la reservation
     *
     * Retourne un tableau d'objets DateTime representant les jours pas disponibles
     * @return array
     */
    public function getBookingDays() {
        $resultat = range(
            $this->arriveDate->getTimestamp(),
            $this->leaveDate->getTimestamp(),
            24 * 60 * 60
        );

        $days = array_map(function($dayTimestamp){
            return new \DateTime(date('Y-m-d', $dayTimestamp));
        }, $resultat);

        return $days;
    }
}
