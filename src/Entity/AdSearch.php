<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class AdSearch
{
    /**
     *
     * @var string|null
     */
    private $city;

    /**
     *
     * @var string|null
     */
    private $country;

    /**
     * @var int|null
     */
    private $beds;

    /**
     * @var int|null
     */
    private $rooms;

    /**
     * @var int|null
     */
    private $bathrooms;

    /**
     * @var ArrayCollection
     */
    private $themes;

    /**
     * @var ArrayCollection
     */
    private $houseTypes;

    /**
     * @var ArrayCollection
     */
    private $equipments;

    public function __construct()
    {
        $this->themes = new ArrayCollection();
        $this->houseTypes = new ArrayCollection();
        $this->equipments = new ArrayCollection();
    }

    /**
     * Get the value of equipements
     *
     * @return  ArrayCollection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Set the value of equipements
     *
     * @param  ArrayCollection  $equipments
     *
     * @return  self
     */
    public function setEquipments(ArrayCollection $equipments)
    {
        $this->equipments = $equipments;

        return $this;
    }

    /**
     * Get the value of thematiques
     *
     * @return  ArrayCollection
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Set the value of thematiques
     *
     * @param  ArrayCollection  $themes
     *
     * @return  self
     */
    public function setThemes(ArrayCollection $themes)
    {
        $this->themes = $themes;

        return $this;
    }


    /**
     * Get the value of typeLogements
     *
     * @return  ArrayCollection
     */
    public function getHouseTypes()
    {
        return $this->houseTypes;
    }

    /**
     * Set the value of typeLogements
     *
     * @param  ArrayCollection  $houseTypes
     *
     * @return  self
     */
    public function setHouseTypes(ArrayCollection $houseTypes)
    {
        $this->houseTypes = $houseTypes;

        return $this;
    }

    /**
     * Get the value of ville
     *
     * @return  string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of ville
     *
     * @param  string|null  $city
     *
     * @return  self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of nbrSdb
     *
     * @return  int|null
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set the value of nbrSdb
     *
     * @param  int|null  $bathrooms
     *
     * @return  self
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;

        return $this;
    }

    /**
     * Get the value of nbrChambres
     *
     * @return  int|null
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set the value of nbrChambres
     *
     * @param  int|null  $rooms
     *
     * @return  self
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get the value of nbrLits
     *
     * @return  int|null
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * Set the value of nbrLits
     *
     * @param  int|null  $beds
     *
     * @return  self
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;

        return $this;
    }

    /**
     * Get the value of pays
     *
     * @return  string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of pays
     *
     * @param  string|null  $country
     *
     * @return  self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }
}
