<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AdRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *      fields={"title"},
 *      message="Il existe déjà une annonce avec le même titre, merci de le modifier"
 * )
 */
class Ad
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *         min = 10,
     *         max = 100,
     *         minMessage = "Le titre de l'annonce doit faire au moins 10 caractères",
     *         maxMessage = "Le titre de l'annonce ne doit pas dépasser les 100 caractères"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Length(
     *         min = 20,
     *         minMessage = "La description de l'annonce doit faire au moins 20 caractères",
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * 
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $country;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0, message = "Vous devez proposer au moins une chambre !")
     */
    private $rooms;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0, message = "Vous devez proposer au moins un lit !")
     * 
     */
    private $beds;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0, message = "Vous devez proposer au moins une salle de bain !")
     * 
     */
    private $bathrooms;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0, message = "Le prix doit être suppérieur à 0")
     * 
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg" })
     */
    private $coverImage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="ad", orphanRemoval=true)
     * @Assert\Valid()
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="ad")
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="ad", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HouseType", inversedBy="ads")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     * 
     */
    private $houseType;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Equipment", mappedBy="ads")
     * @Assert\Count(min = 1, minMessage = "Vous devez renseigner au moins un equipement")
     */
    private $equipments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Theme", mappedBy="ads")
     * @Assert\Count(min = 1, minMessage = "Vous devez renseigner au moins une thématique")
     * 
     */
    private $themes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lon;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="adsLiked")
     * @ORM\JoinTable(name="ad_likes",
     *      joinColumns={@ORM\JoinColumn(name="ad_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $likedBy;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->equipments = new ArrayCollection();
        $this->themes = new ArrayCollection();
        $this->likedBy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getRooms(): ?int
    {
        return $this->rooms;
    }

    public function setRooms(int $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function getBeds(): ?int
    {
        return $this->beds;
    }

    public function setBeds(int $beds): self
    {
        $this->beds = $beds;

        return $this;
    }

    public function getBathrooms(): ?int
    {
        return $this->bathrooms;
    }

    public function setBathrooms(int $bathrooms): self
    {
        $this->bathrooms = $bathrooms;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCoverImage()
    {
        return $this->coverImage;
    }

    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setAd($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getAd() === $this) {
                $image->setAd(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setAd($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getAd() === $this) {
                $booking->setAd(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAd($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAd() === $this) {
                $comment->setAd(null);
            }
        }

        return $this;
    }

    public function getHouseType(): ?HouseType
    {
        return $this->houseType;
    }

    public function setHouseType(?HouseType $houseType): self
    {
        $this->houseType = $houseType;

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
            $equipment->addAd($this);
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        if ($this->equipments->contains($equipment)) {
            $this->equipments->removeElement($equipment);
            $equipment->removeAd($this);
        }

        return $this;
    }

    /**
     * @return Collection|Theme[]
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes[] = $theme;
            $theme->addAd($this);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        if ($this->themes->contains($theme)) {
            $this->themes->removeElement($theme);
            $theme->removeAd($this);
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(?string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?string
    {
        return $this->lon;
    }

    public function setLon(?string $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * @return Collection
     */ 
    public function getLikedBy()
    {
        return $this->likedBy;
    }

    /**
     * Permet de generer le slug de l'annonce
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @return void
     */
    public function initializeSlug() {
        if(empty($this->slug)) {
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->title);
        }
            
    }

    /**
     * Permet de récuperer l'adresse complete
     *
     * @return string
     */
    public function getFullAddress()
    {
        return "{$this->address}, {$this->zipCode} {$this->city}, {$this->country}";
    }

    /**
     * Permet d'obtenir un tableau avec les jours qui ne sont pas disponibles
     * Retourne un tableau d'objets DateTime representant les jours pas disponibles
     * @return array
     */
    public function getNotAvailableDays()
    {
        $notAvailableDays = [];

        foreach ($this->bookings as $booking) {
            // On calcule les jours qui se trouvent entre la date d'arrivée et départ
            $result = range(
                $booking->getArriveDate()->getTimestamp(),
                $booking->getLeaveDate()->getTimestamp(),
                24 * 60 * 60
            );

            $days = array_map(function ($dayTimestamp) {
                return new \DateTime(date('Y-m-d', $dayTimestamp));
            }, $result);

            $notAvailableDays = array_merge($notAvailableDays, $days);
        }

        return $notAvailableDays;
    }

    /**
     * Permet de récupérer la note moyenne d'une annonce
     *
     * @return void
     */
    public function getAvgRating()
    {
        // On calcule la somme des notations
        $sum = array_reduce($this->comments->toArray(), function ($total, $comment) {
            return $total + $comment->getNote();
        }, 0);

        // ON fait la division pour obtenir la moyenne
        if (count($this->comments) > 0) {
            return $sum / count($this->comments);
        } else {
            return 0;
        }
    }

    /**
     * Permet deverifier si une réservation a des comments de l'utilisateur qui est en train de réserver
     *
     * @param User $user
     * @return Comment | null
     */
    public function getCommentFromAuthor(User $author)
    {
        foreach ($this->comments as $comment) {

            if ($comment->getAuthor() === $author) {
                return $comment;
            }
        }

        return null;
    }

    /**
     * Permet de checker si l'annonce et l'utilisateur partagent le ou les mêmes thématiques
     *
     * @param User $user
     * @return bool
     */
    public function getSearchSameTheme(User $user)
    {
        foreach ($this->getThemes() as $adTheme) {
            foreach ($user->getThemes() as $userTheme) {
                if ($adTheme->getName() === $userTheme->getName()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * On obtient la lattitude et longitude de chaque adresse avant de persister ou mettre à jour les données
     * 
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @return void
     */
    public function getCoords() {
        if(empty($this->lat) || empty($this->lon)) {
            $address = $this->getFullAddress();        
            $search = [" ", "'", ","];
            $replace = ["+", "", ""];
            // on convertie l'adresse en lower-case, on enleve les elements du tableau $search et on les remplace par ceux du $replace
            // On convertie : 3 rue du champion, 34000 Montpellier, France ===> 3+rue+du+champion+34000+montpellier+france
            $adresseUrl = strtolower(str_replace($search, $replace, $address));
            
            // On appel l'url externe avec l'adresse mise au bon format et on recupere le JSOn, qu'on converttie en array et on récupére les données qui nous interesent
            $client = HttpClient::create();
            $response = $client->request('GET', "https://nominatim.openstreetmap.org/?addressdetails=1&q=$adresseUrl&format=json&limit=1");
            $result = $response->toArray();

            $this->lat = $result[0]['lat'];
            $this->lon = $result[0]['lon'];
        }
    }    

    public function like(User $user) {
        if($this->likedBy->contains($user)) {
            return;
        }
        $this->likedBy->add($user);
    }

}
