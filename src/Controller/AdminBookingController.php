<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\AdminBookingType;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\BookingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdminBookingController extends AbstractController
{
    /**
     * Permet d'afficher la liste des réservations
     * 
     * @Route("/admin/booking/{page<\d+>?1}", name="admin_booking_index")
     * 
     * @param PaginationService $pagination
     * @param int $page
     * @return Response
     * 
     */
    public function index(PaginationService $pagination, $page)
    {
        $pagination->setEntityClass(Booking::class)
            ->setPage($page);

        return $this->render('admin/booking/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet à l'administrateur d'éditer une réservation
     * 
     * @Route("/admin/booking/{id}/edit", name="admin_booking_edit")
     *
     * @param Booking $booking
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, Booking $booking, EntityManagerInterface $manager)
    {
        $form = $this->createForm(AdminBookingType::class, $booking);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On remet le montant total à 0, de cette façon, avant le persist le montant total sera calculé grâce à la fonction prePersist() de l'entité Rservation
            $booking->setTotalPrice(0);

            $manager->persist($booking);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications de la réservation n° {$booking->getId()} ont bien été prises en compte"
            );

            return $this->redirectToRoute('admin_booking_index');
        }

        return $this->render('admin/booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet à l'administrateur de supprimer une réservation
     *
     * @Route("/admin/booking/{id}/delete", name="admin_booking_delete")
     * 
     * @param Booking $booking
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function delete(Booking $booking, EntityManagerInterface $manager)
    {

        $manager->remove($booking);
        $manager->flush();

        $this->addFlash(
            'success',
            "La réservation n° {$booking->getId()} a bien été supprimée"
        );

        return $this->redirectToRoute('admin_booking_index');
    }
}
