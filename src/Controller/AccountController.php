<?php

namespace App\Controller;

use App\Form\AccountType;
use App\Entity\User;
use App\Entity\PasswordUpdate;
use App\Form\RegistrationType;
use App\Form\PasswordUpdateType;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * Permet de se connecter
     * 
     * @Route("/login", name="account_login")
     * 
     * @param AuthenticationUtils $utils
     */
    public function login(AuthenticationUtils $utils)
    {
        // Permet de récupérer l'erreur d'authetification et afficher les messages d'erreur sur la page login
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        // hasError va stocker l'erreur en questioon, sinon par defaut on le met à null
        return $this->render('account/login.html.twig', [
            'hasError' => $error !== null,
            'username' => $username
        ]);
    }

    /**
     * Permet de se déconnecter
     * 
     * @Route("/logout", name="account_logout")
     *
     * @return void
     */
    public function logout() {

    }

    /**
     * Permet de s'enregistrer et créer un compte
     *
     * @Route("/registration", name="account_registration")
     * 
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface $manager
     * 
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager) {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On crypte le mot de passe que l'utilisateur saisie dans le formulaire
            $hash = $encoder->encodePassword($user, $user->getPassword());
            // On stocke le mot de passe crypté
            $user->setPassword($hash);

            // On ajoute les thématiques aux utilisateurs
            foreach ($user->getThemes() as $theme) {
                $theme->addUser($user);
                $manager->persist($theme);
            }

            // On fait appel au manager de Doctrine on persiste les données et on les envoie dans la BDD
            $manager->persist($user);
            $manager->flush();

            // On affiche un message flash si on crée le compte avec succès
            $this->addFlash(
                'success',
                'Vous avez créé votre compte avec succès, bravo !'
            );

            // On est renvoyé vers la page de login
            return $this->redirectToRoute('account_login');
        }

        return $this->render('account/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher la page profil de l'utilisateur connecté
     *
     * @Route("/account", name="account_show")
     * @IsGranted("ROLE_USER")
     * 
     * @return Response
     */
    public function myAccount(){

        // On affiche la même page que "utilisateur_show" avec l'utilisateur qui est actuellement connecté(on l'obtient avec $this->getUser())
        return $this->render('user/show.html.twig', [
            'user' => $this->getUser()
        ]);

    }

    /**
     * Permet d'afficher le formulaire d'edition de compte
     *
     * @Route("/account/profile", name="account_profile")
     * @IsGranted("ROLE_USER")
     * 
     * @param Request $request
     * @param EntityManagerInterface $manager
     * 
     * @return Response
     */
    public function profile(Request $request, EntityManagerInterface $manager) {
        $user = $this->getUser();

        $form = $this->createForm(AccountType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            // On ajoute les thématiques aux users
            foreach ($user->getThemes() as $theme) {
                $theme->addUser($user);
                $manager->persist($theme);
            }
            
            $manager->persist($user);
            $manager->flush();

            $this->addFlash(
                'success',
                'Votre compte a bien été modifié !'
            );
        }

        return $this->render('account/profile.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet de mettre à jour le mot de passe
     *
     * @Route("/account/password-update", name="account_password")
     * @IsGranted("ROLE_USER")
     * 
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager) {
        $passwordUpdate = new PasswordUpdate();

        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On verifie si le oldPassword est le même que le mot de passe de l'utilisateur connecté
            if(!password_verify($passwordUpdate->getOldPassword(), $user->getPassword())) {
                // On gére l'erreur et on creée un message d'erreur personnalisé
                $form->get('oldPassword')->addError(new FormError("Le mot de passe que vous avez saisie ne correspond pas a votre mot de passe actuel"));

            } else {
                $newPassword = $passwordUpdate->getNewPassword();
                
                $hash = $encoder->encodePassword($user, $newPassword);
                $user->setPassword($hash);

                $manager->persist($user);
                $manager->flush();

                $this->addFlash(
                    'success',
                    'Le mot de passe a bien été modifié'
                );

                return $this->redirectToRoute('ad_index');
            }
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * Permet d'afficher la liste des reservations d'un utilisateur
     * 
     * @Route("/account/bookings", name="account_bookings")
     * @IsGranted("ROLE_USER")
     *
     * @return void
     */
    public function bookings() {
        return $this->render('account/bookings.html.twig');
    }

    /**
     * Permet d'afficher la liste des annonces favorits(likées)
     * 
     * @Route("/account/favorites", name="account_favorites")
     * @IsGranted("ROLE_USER")
     *
     * @return void
     */
    public function favorites() {
        return $this->render('account/favorites.html.twig');
    }
}
