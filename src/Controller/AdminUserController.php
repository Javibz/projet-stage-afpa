<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/user/{page<\d+>?1}", name="admin_user_index")
     * @param PaginationService $pagination
     * @param int $page
     * @return Response
     */
    public function index(PaginationService $pagination, $page)
    {
        $pagination->setEntityClass(User::class)
            ->setPage($page);
            
        return $this->render('admin/user/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet d'éditer le profil d'un utilisateur
     *
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     * 
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, User $user, EntityManagerInterface $manager){

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $manager->persist($user);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications de l'utilisateur' <strong>{$user->getFullName()}</strong> on bien été prises en compte"
            );

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Permet à l'administrateur de supprimer un utilisateur
     * 
     * @Route("/admin/user/{id}/delete", name="admin_user_delete")
     *
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function delete(User $user, EntityManagerInterface $manager) {

        if (count($user->getBookings()) > 0 && count($user->getAds()) > 0 && count($user->getComments()) > 0) {
            $this->addFlash(
                'danger',
                "<strong>Attention !!</strong> Vous ne pouvez pas supprimer un utilisateur qui a des annonces, réservations et commentaires."
            );
        } else {

            $manager->remove($user);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'utilisateur <strong>{$user->getFullName()}</strong> a bien été supprimée"
            );
        }

        return $this->redirectToRoute('admin_user_index');
    }
}
