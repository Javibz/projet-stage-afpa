<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AnnonceType;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminAdController extends AbstractController
{
    /**
     * Permet d'afficher la liste de toutes les annonces.
     * 
     * @Route("/admin/ad/{page<\d+>?1}", name="admin_ad_index")
     * 
     * @param PaginationService $pagination
     * @param int $page
     * @return Response
     * 
     */
    public function index(PaginationService $pagination, $page)
    {
        $pagination->setEntityClass(Ad::class)
                   ->setPage($page);

        return $this->render('admin/ad/index.html.twig', [
            'pagination' => $pagination,

        ]);
    }

    /**
     * Permet à l'administrateur d'éditer une annonce
     * 
     * @Route("/admin/ad/{id}/edit", name="admin_ad_edit")
     *
     * @param Request $request
     * @param Ad $ad
     * @param EntityManagerInterface $manager
     * 
     * @return Response
     */
    public function edit(Request $request, Ad $ad, EntityManagerInterface $manager){

        $form = $this->createForm(AnnonceType::class, $ad);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications de l'annonce <strong>{$ad->getTitle()}</strong> on bien été prises en compte"
            );

            return $this->redirectToRoute('admin_ad_index');

        }

        return $this->render('admin/ad/edit.html.twig', [
            'ad' => $ad,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Permet à l'administrateur de supprimer une annonce
     * 
     * @Route("admin/ad/{id}/delete", name="admin_ad_delete")
     *
     * @param Ad $ad
     * @param EntityManagerInterface $manager
     * @return void
     * 
     */
    public function delete(Ad $ad, EntityManagerInterface $manager) {
        if(count($ad->getBookings()) > 0 ) {
            $this->addFlash(
                'warning',
                "Attention !! Vous ne pouvez pas supprimer une annonce qui a des réservations"
            );
        } else {

            $manager->remove($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "Vous avez supprimé correctemen l'annonce <strong>{$ad->getTitle()}</strong>"
            );
        }

        return $this->redirectToRoute('admin_ad_index');
    }
}
