<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AnnonceType;
use App\Entity\AdSearch;
use App\Form\AdSearchType;
use App\Repository\AdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    /**
     * Permet d'afficher la liste de toutes les annonces
     * 
     * @Route("/index", name="ad_index")
     * 
     * @param AdRepository $adRepo
     * @param Request $request
     * @return Response
     */
    public function index(AdRepository $adRepo, Request $request)
    {
        $search = new AdSearch();
        $form = $this->createForm(AdSearchType::class, $search);
        $form->handleRequest($request);
        
        return $this->render('ad/index.html.twig', [
            'ads' => $adRepo->findAllWithFilters($search),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Permet de créer une nouvelle annonce
     *
     * @Route("/ad/create", name="ad_create")
     * @IsGranted("ROLE_USER")
     * 
     * @param Request $requete
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $manager) {
        $ad = new Ad();

        $form = $this->createForm(AnnonceType::class, $ad);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $file = $ad->getCoverImage();
            $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

            // moves the file to the directory where images are stored
            $file->move(
                $this->getParameter('images_directory'),
                $fileName
            );

            $ad->setCoverImage($fileName);
            
            // On ajoute l'annonce aux images
            foreach($ad->getImages() as $image) {
                $i = 1;
                // on obtinet les infos du champ url
                $imagePath = $image->getUrl();

                // on récupére le nom du fichier
                $imageFile = $imagePath[0];

                // on genere un nom de fichier unique
                $imageFileName = $i . '-' . $this->generateUniqueFileName() .'.' . $imageFile->guessExtension();

                // on envoie le fichier vers le dossier specifié dans les parametres de services.yaml
                $imageFile->move(
                    $this->getParameter('images_directory'),
                    $imageFileName
                );

                $image->setUrl($imageFileName);

                $image->setAd($ad);
                $manager->persist($image);

                $i++;
            }
            
            // On ajoute l'annonce aux equipements
            foreach ($ad->getEquipments() as $equipment) {
                $equipment->addAd($ad);
                $manager->persist($equipment);
            }
            
            // On ajoute l'annonce aux thématiques
            foreach ($ad->getThemes() as $theme) {
                $theme->addAd($ad);
                $manager->persist($theme);
            }

            $ad->setAuthor($this->getUser());

            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce : {$ad->getTitle()}, à bien été crée !"
            );

            return $this->redirectToRoute("ad_show", [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet de modifier une annonce
     * 
     * @Route("/ad/{slug}/edit", name="ad_edit")
     * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()", message="Cette annonce ne vous appartient pas, vous ne pouvez pas la modifier !")
     *
     * @param Request $requete
     * @param EntityManagerInterface $manager
     * @param Ad $ad
     * @return Response
     */
    public function edit(Request $request, Ad $ad, EntityManagerInterface $manager) {
        $form = $this->createForm(AnnonceType::class, $ad);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $file = $ad->getCoverImage();

            $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

            // moves the file to the directory where images are stored
            $file->move(
                $this->getParameter('images_directory'),
                $fileName
            );

            $ad->setCoverImage($fileName);
            
            // On ajoute l'annonce aux images
            foreach($ad->getImages() as $image) {
                $i = 1;
                // on obtinet les infos du champ url
                $imagePath = $image->getUrl();

                // on récupére le nom du fichier
                $imageFile = $imagePath[0];

                // on genere un nom de fichier unique
                $imageFileName = $i . '-' . $this->generateUniqueFileName() . '.' . $imageFile->guessExtension();

                // on envoie le fichier vers le dossier specifié dans les parametres de services.yaml
                $imageFile->move(
                    $this->getParameter('images_directory'),
                    $imageFileName
                );

                $image->setUrl($imageFileName);

                $image->setAd($ad);
                $manager->persist($image);

                $i++;
            }

            // On ajoute l'annonce aux equipements
            foreach($ad->getEquipments() as $equipment) {
                $equipment->addAd($ad);
                $manager->persist($equipment);
            }

            // On ajoute l'annonce aux thématiques
            foreach($ad->getThemes() as $theme) {
                $theme->addAd($ad);
                $manager->persist($theme);
            }

            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce : {$ad->getTitle()} a bien été modifié !"
            );

            return $this->redirectToRoute('ad_show', [
                "slug" => $ad->getSlug()
            ]);
        }

        return $this->render('ad/edit.html.twig', [
            'form' => $form->createView(),
            'ad' => $ad
        ]);
    }

    /**
     * Permet d'afficher les détails d'une annonce
     * 
     * @Route("/ad/{slug}", name="ad_show")
     *
     * @param Ad $ad
     * @return Response
     */
    public function show(Ad $ad) {
        
        return $this->render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    }

    /**
     * Permet de supprimer une annonce
     * 
     * @Route("/ad/{slug}/delete", name="ad_delete")
     * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()", message="Cette annonce ne vous appartient pas, impossible de satisfaire votre demande!!")
     * 
     * @param Ad $ad
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Ad $ad, EntityManagerInterface $manager){
        $manager->remove($ad);
        $manager->flush();

        $this->addFlash(
            'danger',
            "Vous avez supprimé l'annonce avec succès"
        );

        return $this->redirectToRoute('ad_index');
    }

    /**
     * Permet de generer un nom de fichier crypté et unique
     * 
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
