<?php

namespace App\Controller;

use App\Repository\AdRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="homepage")
     * @Route("/", name="default_page")
     * 
     * @param AdRepository $repoAd
     * @param UserRepository $repoUser
     * @return Response
     * 
     */
    public function index(AdRepository $repoAd, UserRepository $repoUser)
    {
        
        return $this->render('home/index.html.twig', [
            'ads' => $repoAd->findTopAds(6),
            'users' => $repoUser->findTopUsers(3),
            'themes1' => $repoAd->findByTheme('Sport', 3),
            'themes2' => $repoAd->findByTheme('Literature', 3),
            'themes3' => $repoAd->findByTheme('Jeux-vidéos', 3),
            'houseTypes' => $repoAd->findByHouseType('villa', 3),
            'countries' => $repoAd->findBy(['country' => 'France'], ['city' => 'ASC'], 3)
        ]);
    }
}
