<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Comment;
use App\Entity\Booking;
use App\Form\CommentType;
use App\Form\BookingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookingController extends AbstractController
{
    /**
     * @Route("/ad/{slug}/book", name="booking_create")
     * @IsGranted("ROLE_USER")
     * 
     * @param MailerInterface $mailer
     * @param Ad $ad
     * @param Request $request
     * @param EntityManagerInterface $manager
     * 
     * @return Response
     * 
     */
    public function book(MailerInterface $mailer, Ad $ad, Request $request, EntityManagerInterface $manager)
    {
        $booking = new Booking();

        $form = $this->createForm(BookingType::class, $booking);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();

            $booking->setBooker($user)
                        ->setAd($ad);

            // Si les dates ne sont pas disponibles on affiche un message d'erreur
            if(!$booking->isBookedDates()) {
                $this->addFlash(
                    'danger',
                    'Les dates que vous avez choisie ne sont pas disponibles, changez de dates svp.'
                );
            } else {

                $manager->persist($booking);
                $manager->flush();
                
                $email = new TemplatedEmail();

                $email->from('javiTestMail@example.com')
                      ->to($user->getEmail())
                      ->subject("Reservation n° {$booking->getId()}")
                      ->htmlTemplate("email/bookingConfirm.html.twig")
                      ->context([
                          'booking' => $booking,
                          'ad' => $booking->getAd(),
                          'user' =>$user
                      ]);
                
                $mailer->send($email);
                
                return $this->redirectToRoute('booking_show', ['id' => $booking->getId(), 'whithAlert' => true]);
            }

        }
        
        return $this->render('booking/book.html.twig', [
            'ad' => $ad,
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher le detail d'une reservation
     *
     * @Route("booking/{id}", name="booking_show")
     * 
     * @param Booking $booking
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function show(Request $request, Booking $booking, EntityManagerInterface $manager) {

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $comment->setAd($booking->getAd())
                        ->setAuthor($this->getUser());
            
            $manager->persist($comment);

            $manager->flush();

            $this->addFlash(
                'success',
                'Votre commentaire a bien été pris en compte !'
            );
        }

        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'form' => $form->createView()
        ]);
    }
}
