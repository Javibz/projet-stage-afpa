<?php

namespace App\Controller;

use App\Repository\ImageRepository;
use App\Service\StatsService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDashboardController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     * 
     * @param StatsService $statsService
     * @return Response
     */
    public function index(StatsService $statsService, ImageRepository $imageRepo)
    {
        $stats = $statsService->getStats();
        $topAds = $statsService->getAdsStats('DESC', 5);
        $worstAds = $statsService->getAdsStats('ASC', 5);

        return $this->render('admin/dashboard/index.html.twig', [
            'stats' => $stats,
            'topAds' => $topAds,
            'worstAds' => $worstAds,
            'images'=> $imageRepo->findAll()
        ]);
    }
}
