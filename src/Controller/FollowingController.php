<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/following")
 */
class FollowingController extends AbstractController
{
    /**
     * @Route("/follow/{slug}", name="following_follow")
     */
    public function follow(User $userToFollow, EntityManagerInterface $em)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        
        if($userToFollow->getId() !== $currentUser->getId()) {
            $currentUser->getFollowing()->add($userToFollow);
    
            $em->flush();
    
            return $this->redirectToRoute("user_show", [
                "slug" => $userToFollow->getSlug()
            ]);
        } else {
            return $this->addFlash(
                'danger',
                'Vous ne pouvez pas vous suivre à vous même !!'
            );
        }
    }

     /**
     * @Route("/unfollow/{slug}", name="following_unfollow")
     */
    public function unfollow(User $userToUnfollow, EntityManagerInterface $em)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $currentUser->getFollowing()->removeElement($userToUnfollow);

        $em->flush();

        return $this->redirectToRoute("user_show", [
            "slug" => $userToUnfollow->getSlug()
        ]);
    }
}
