<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    /**
     * Permet d'afficher la liste de tous les commentaires
     * 
     * @Route("/admin/comment/{page<\d+>?1}", name="admin_comment_index")
     * 
     * @param PaginationService $pagination
     * @param int $page
     * @return Response
     * 
     */
    public function index(PaginationService $pagination, $page)
    {
        $pagination->setEntityClass(Comment::class)
            ->setPage($page);

        return $this->render('admin/comment/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet à l'administrateur de modifier un commentaire
     * 
     * @Route("/admin/comment/{id}/edit", name="admin_comment_edit")
     * 
     * @param Request $request
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, Comment $comment, EntityManagerInterface $manager) {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Les modifications du commentaire <strong>n° {$comment->getId()}</strong> poublié par <strong>{$comment->getAuthor()->getFullName()}</strong> ont bien été prises en compte"
            );

            return $this->redirectToRoute('admin_comment_index');
        }

        return $this->render('admin/comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Pemet à l'administrateur de supprimer un commentaire
     *
     * @Route("/admin/comment/{id}/delete", name="admin_comment_delete")
     * 
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function delete(Comment $comment, EntityManagerInterface $manager){

            $manager->remove($comment);
            $manager->flush();

            $this->addFlash(
                'success',
                "Vous avez supprimé correctement le commentaire <strong>{$comment->getId()}</strong> de {$comment->getAuthor()->getFullName()} !"
            );

        return $this->redirectToRoute('admin_comment_index');
    }
}
