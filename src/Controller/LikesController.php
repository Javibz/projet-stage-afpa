<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/likes")
 */
class LikesController extends AbstractController
{
    /**
     * @Route("/like/{id}", name="likes_like")
     * @return JsonResponse
     */
    public function like(Ad $ad, EntityManagerInterface $em)
    {
        $currentUser = $this->getUser();

        if(!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $ad->like($currentUser);
        $em->flush();

        return new JsonResponse([
            'count' => $ad->getLikedBy()->count()
        ]);
    }

    /**
     * @Route("/unlike/{id}", name="likes_unlike")
     * @return JsonResponse
     */
    public function unlike(Ad $ad, EntityManagerInterface $em)
    {
        $currentUser = $this->getUser();

        if(!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $ad->getLikedBy()->removeElement($currentUser);
        $em->flush();

        return new JsonResponse([
            'count' => $ad->getLikedBy()->count()
        ]);
    }
}
