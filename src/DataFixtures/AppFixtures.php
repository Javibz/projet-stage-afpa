<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Role;
use App\Entity\Image;
use App\Entity\Ad;
use App\Entity\Equipment;
use App\Entity\Theme;
use App\Entity\Comment;
use App\Entity\Booking;
use App\Entity\User;
use App\Entity\HouseType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');
        
        // Création des thématiques
        $themesList = ['Sport', 'Musique', 'Literature', 'Yoga', 'Gastronomie', 'Culture', 'Arts du cirque', 'Art', 'Jeux-vidéos', 'Informatique', 'Ecologie'];
        $themes = [];

        for ($h = 0; $h < count($themesList); $h++) {
            $theme = new Theme();

            $theme->setName($themesList[$h])
                  ->setDescription($faker->paragraph());

            $manager->persist($theme);

            $themes[] = $theme;
        }

        // Creation de l'utilisateur administrateur
        $roleAdmin = new Role();
        $roleAdmin->setTitle('ROLE_ADMIN');
        $manager->persist($roleAdmin);

        $admin = new User();
        $admin->setNickName("AdminSymfo")
            ->setLastName('Ramon')
            ->setFirstName('Javi')
            ->setEmail('symfo@symfony.com')
            ->setPassword($this->encoder->encodePassword($admin, 'password'))
            ->setPhone($faker->e164PhoneNumber())
            ->setAddress($faker->streetAddress())
            ->setZipCode($faker->postcode())
            ->setCity($faker->city())
            ->setCountry($faker->country())
            ->setDescription('<p>' . join('</p><p>', $faker->paragraphs(2)) . '</p>')
            ->setBirthDate($faker->dateTime('now', null))
            ->setAvatar('https://randomuser.me/api/portraits/lego/0.jpg')
            ->addUserRole($roleAdmin);
        
        $manager->persist($admin);
        
        // Creations des utilisateurs
        $users = [];
        $gender = ['male','female'];

        for($i = 1; $i < 15; $i++) {
            $user = new User();

            $hash = $this->encoder->encodePassword($user, "password");

            $genre = $faker->randomElement($gender);
            $avatar = 'https://randomuser.me/api/portraits/';
            $avatarNumber = $faker->numberBetween(1,99) . '.jpg';

            $avatar .= ($genre == 'male' ? 'men/' : 'women/') . $avatarNumber;
             
    
            $user->setNickName($faker->userName())
                        ->setLastName($faker->lastName())
                        ->setFirstName($faker->firstName(null))
                        ->setEmail($faker->email())
                        ->setPassword($hash)
                        ->setPhone($faker->e164PhoneNumber())
                        ->setAddress($faker->streetAddress())
                        ->setZipCode($faker->postcode())
                        ->setCity($faker->city())
                        ->setCountry($faker->country())
                        ->setDescription('<p>' . join('</p><p>', $faker->paragraphs(5)) . '</p>')
                        ->setBirthDate($faker->dateTime('now', null))
                        ->setAvatar($avatar);

            for ($th = 0; $th < mt_rand(1, 3); $th++) {
                $user->addTheme($themes[mt_rand(0, count($themes) - 1)]);
            }        
            
            $manager->persist($user);
            $users[] = $user;
        }

        // Création des types de logement
        $houseTypeNames = ['Studio', 'Maison', 'Villa', 'Appartement', 'Bungalow', 'Atypique'];
        $houseTypes = [];

        for($t = 0; $t < count($houseTypeNames) -1; $t++) {
            $houseType = new HouseType();

            $houseType->setName($houseTypeNames[$t]);

            $manager->persist($houseType);
            $houseTypes[] = $houseType;

        }

        // Création des equipements
        $nomEquipments = ['Wi-Fi', 'Ascenseur', 'Climatisation', 'Chauffage', 'Télévision', 'Fer à repasser', 'Cuisine', 'Lave-linge', 'Parking', 'Cafetière', 'Sèche-cheveux'];
        $equipments = [];

        for ($e = 0; $e < count($nomEquipments) - 1; $e++) {
            $equipment = new Equipment();

            $equipment->setName($nomEquipments[$e]);

            $manager->persist($equipment);
            $equipments[] = $equipment;
        }

        // Création des annonces
        for($k = 0; $k < 20; $k++) {
            $ad = new Ad();

            $title = $faker->sentence();
            $description = '<p>' . join('</p><p>', $faker->paragraphs(5)) .'</p>';
            
            $addresses = ['1 Rue Bosquet', '3 Rue des Bengalis', '30 Route de Laverune', '178 Rue de la Carrierasse', '1 Rue de Provence', '1 Rue des Acacias', '50 Rue Jaufre Rudel', '114 Impasse du Roc Blanc', '72 Rue des Sauges', '3 Rue Pierre de Vernols', '180 Rue du Caducee', '305 Rue Dante Alighieri', '32 Chemin des Terebinthes', '2 Allee de l\'Amellan', '2 Rue du Saule', '24 Rue de la Bandido', '375 Avenue Leonard de Vinci', '7 Rue du Cinsault', '4 Rue des Pinsons', '100 Allee Alberto Giacometti'];

            $zipCodes = ['34090', '34090', '34070', '34090', '34170', '34920', '34080', '34070', '34000', '34070', '34090', '34790', '34830', '34170', '34920', '34000', '34970', '34970', '34430', '34000'];
            
            $cities = ['Montpellier', 'Montpellier', 'Montpellier', 'Montpellier', 'Castelnau-le-Lez', 'Le Cres', 'Montpellier', 'Montpellier', 'Montpellier', 'Montpellier', 'Montpellier', 'Grabels', 'Clapiers', 'Castelnau-le-Lez', 'Le Cres', 'Montpellier', 'Lattes', 'Lattes', 'Saint-Jean-de-Vedas', 'Montpellier'];

            $user = $users[mt_rand(0, count($users) - 1)];

            $houseType = $houseTypes[mt_rand(0, count($houseTypes) - 1)];
            
            // $thematique = $thematiques[mt_rand(0, count($thematiques) - 1)];

            $ad->setTitle($title)
                    ->setDescription($description)
                    ->setAddress($addresses[$k])
                    ->setZipCode($zipCodes[$k])
                    ->setCity($cities[$k])
                    ->setCountry('France')
                    ->setRooms(mt_rand(1, 6))
                    ->setBeds(mt_rand(1, 5))
                    ->setBathrooms(mt_rand(1, 6))
                    ->setPrice(mt_rand(10, 100))
                    ->setCapacity(mt_rand(1, 6))
                    ->setCoverImage('ad-image'. mt_rand(1, 4). '.jpg')
                    ->setAuthor($user)
                    ->setHouseType($houseType);

            // Creation des images lies a une annonce
            for($j = 0; $j < mt_rand(2, 6); $j++){

                $image = new Image();

                $image->setTitle($title)
                      ->setUrl('ad-image' . mt_rand(1, 4) . '.jpg')
                      ->setAd($ad);
                
                $manager->persist($image);
            }

            // Ajout des equipements dans chaque annonce
            for ($eq = 0; $eq < mt_rand(3, count($equipments) - 1); $eq++) {

                $ad->addEquipment($equipments[$eq]);
            }

            for ($th = 0; $th < mt_rand(1, 3); $th++) {
                $ad->addTheme($themes[mt_rand(0, count($themes) -1)]);
            }
          
        
            // Creation des reservations
            for($p = 0; $p < 5; $p++){
                $booking = new Booking();
                
                $createdAt = $faker->dateTimeBetween('-6 months');
                $arriveDate = $faker->dateTimeBetween('-3 months');
                
                $days = mt_rand(2, 8);
                
                // On crée un clon de la variable pour ne pas ecraser l'initial
                $leaveDate = (clone $arriveDate)->modify("+$days days");
                
                $totalPrice = $ad->getPrice() * $days;

                $booker = $users[mt_rand(0, count($users) - 1)];
                
                $booking->setBooker($booker)
                ->setAd($ad)
                ->setArriveDate($arriveDate)
                ->setLeaveDate($leaveDate)
                ->setCreatedAt($createdAt)
                ->setComment($faker->paragraph())
                ->setTotalPrice($totalPrice)
                ->setTravelers(mt_rand(1, $ad->getCapacity()));
                
                $manager->persist($booking);
                
                // Creaton et ajout des commentaires dans les reservations
                if(mt_rand(0, 1)) {
                    $comment = new Comment();
                    
                    $comment->setContent($faker->paragraph())
                    ->setNote(mt_rand(1, 5))
                    ->setAuthor($booker)
                    ->setAd($ad);
                    
                    $manager->persist($comment);
                }
            }

            $manager->persist($ad);
        }

        $manager->flush();
    }
}
