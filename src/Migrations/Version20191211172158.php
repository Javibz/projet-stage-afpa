<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191211172158 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE thematique (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thematique_utilisateur (thematique_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_8024AC476556AF (thematique_id), INDEX IDX_8024ACFB88E14F (utilisateur_id), PRIMARY KEY(thematique_id, utilisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thematique_annonce (thematique_id INT NOT NULL, annonce_id INT NOT NULL, INDEX IDX_CD6577CC476556AF (thematique_id), INDEX IDX_CD6577CC8805AB2F (annonce_id), PRIMARY KEY(thematique_id, annonce_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE thematique_utilisateur ADD CONSTRAINT FK_8024AC476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE thematique_utilisateur ADD CONSTRAINT FK_8024ACFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE thematique_annonce ADD CONSTRAINT FK_CD6577CC476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE thematique_annonce ADD CONSTRAINT FK_CD6577CC8805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE thematique_utilisateur DROP FOREIGN KEY FK_8024AC476556AF');
        $this->addSql('ALTER TABLE thematique_annonce DROP FOREIGN KEY FK_CD6577CC476556AF');
        $this->addSql('DROP TABLE thematique');
        $this->addSql('DROP TABLE thematique_utilisateur');
        $this->addSql('DROP TABLE thematique_annonce');
    }
}
