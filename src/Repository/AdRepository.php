<?php

namespace App\Repository;

use App\Entity\Ad;
use App\Entity\AdSearch;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Ad|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ad|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ad[]    findAll()
 * @method Ad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ad::class);
    }

    /**
     * Permet de faire un recherche avec des filtres
     *
     * @param AdSearch $search
     * 
     * @return Ad[] Returns an array of Ad objects
     */
    public function findAllWithFilters(AdSearch $search)
    {

        $query = $this->createQueryBuilder('a');

        if (!empty($search->getCity())) {
            $query = $query->andwhere('a.city = :city')
                ->setParameter('city', $search->getCity());
        }

        if (!empty($search->getCountry())) {
            $query = $query->andwhere('a.country = :country')
                ->setParameter('country', $search->getCountry());
        }

        if (!empty($search->getRooms())) {
            $query = $query->andwhere('a.rooms >= :rooms')
                ->setParameter('rooms', $search->getRooms());
        }

        if (!empty($search->getBeds())) {
            $query = $query->andwhere('a.beds >= :beds')
                ->setParameter('beds', $search->getBeds());
        }

        if (!empty($search->getBathrooms())) {
            $query = $query->andwhere('a.bathrooms >= :bathroomss')
                ->setParameter('bathroomss', $search->getBathrooms());
        }

        if (!empty($search->getThemes()->count() > 0)) {
            $k = 0;
            foreach ($search->getThemes() as $theme) {
                $k++;
                $query = $query->andwhere(":theme$k MEMBER OF a.themes")
                    ->setParameter("theme$k", $theme);
            }
        }

        if (!empty($search->getEquipments()->count() > 0)) {
            $k = 0;
            foreach ($search->getEquipments() as $equipment) {
                $k++;
                $query = $query->andwhere(":equip$k MEMBER OF a.equipments")
                    ->setParameter("equip$k", $equipment);
            }
        }

        if (!empty($search->getHouseTypes()->count() > 0)) {
            $k = 0;
            foreach ($search->getHouseTypes() as $houseType) {
                $k++;
                $query = $query->andwhere(":houseTyp$k = a.houseType")
                    ->setParameter("houseTyp$k", $houseType);
            }
        }

        return $query->getQuery()->getResult();
    }


    // SELECT ad.title, AVG(comment.note) as avgRating FROM `ad`
    // INNER JOIN comment on comment.author_id = ad.id
    // GROUP BY ad.title
    // ORDER BY avgRating DESC
    // LIMIT 6
    //
    // Requête pour obtenir le top X des meilleurs annonces
    public function findTopAds($limit) {
        return $this->createQueryBuilder('a')
                    ->select('a as ad, AVG(c.note) as avgRating')
                    ->join('a.comments', 'c')
                    ->groupBy('a')
                    ->orderBy('avgRating', 'DESC')
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult();
    }

    // Traduction en requête SQL :
    // SELECT * FROM annonce
    // INNER JOIN thematique_annonce on thematique_annonce.annonce_id = annonce.id
    // INNER JOIN thematique on thematique.id = thematique_annonce.thematique_id
    // WHERE annonce.id = thematique_annonce.annonce_id
    // AND thematique.nom = 'Musique'
    /**
     * Permet d'afficher les annonces par thematique
     * 
     * @param string $value
     * @param integer|null $limit
     * @return Ad[] Returns an array of Ad objects
     * 
     */
    public function findByTheme($value, $limit = null)
    {
        return $this->createQueryBuilder('a')
                    ->select('a as ad')
                    ->join('a.themes', 'ta')
                    ->where('ta.name = :val')
                    ->setParameter('val', $value)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Permet d'afficher les annonces par type de logement
     * 
     * @param string $value
     * @param integer|null $limit
     * @return Ad[] Returns an array of Ad objects
     * 
     */
    public function findByHouseType($value, $limit = null)
    {
        return $this->createQueryBuilder('a')
                    ->select('a as ad')
                    ->join('a.houseType', 'tl')
                    ->where('tl.name = :val')
                    ->setParameter('val', $value)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Ad
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
