INSERT INTO `ad` (`title`, `description`, `address`, `zip_code`, `city`, `country`, `rooms`, `beds`, `bathrooms`, `price`, `cover_image`, `slug`, `capacity`, `lat`, `lon`, `author_id`, `house_type_id`) 
VALUES ("Une annonce de test cool", "Une annonce de test cool Une [...]", "146 Rue de la Croix de Las Cazes", "34087", "Montpellier" , "France", 4, 7, 10, 200,  "059357d61d29a521a5.jpg", "une-annonce-de-test-cool", 6, "43.6177638", "3.8517631", 11, 2)

UPDATE `ad` SET (`title`="Une annonce de test cool modifié") 
WHERE `id`=21


DELETE FROM `ad` WHERE `id`=21

DELIMITER |

  CREATE PROCEDURE add_insert_join_tables (
    IN p_title_ad VARCHAR(255), 
    IN p_description_ad LONGTEXT, 
    IN p_address_ad VARCHAR(255), 
    IN p_zip_code_ad VARCHAR(255), 
    IN p_city_ad VARCHAR(255), 
    IN p_country_ad VARCHAR(255), 
    IN p_rooms_ad INT(11), 
    IN p_beds_ad INT(11), 
    IN p_bathrooms_ad INT(11), 
    IN p_price_ad INT(11), 
    IN p_cover_image_ad VARCHAR(255), 
    IN p_slug_ad VARCHAR(255), 
    IN p_capacity_ad INT(11), 
    IN p_lat_ad VARCHAR(255), 
    IN p_lon_ad VARCHAR(255), 
    IN p_author_id_ad INT(11), 
    IN p_house_type_id_ad INT(11), 
    IN p_title_image VARCHAR(255), 
    IN p_url_image VARCHAR(255), 
    IN p_equipment_id INT(11), 
    IN p_theme_id INT(11), 
    OUT p_ad_id INT(11))

BEGIN

	INSERT INTO `ad` (title, description, address, zip_code, city, country, rooms, beds, 	bathrooms, price, cover_image, slug, capacity, lat, lon, author_id, house_type_id) 	VALUES (p_title_ad, p_description_ad, p_address_ad, p_zip_code_ad, p_city_ad, 	p_country_ad, p_rooms_ad, p_beds_ad, p_bathrooms_ad, p_price_ad, 	p_cover_image_ad, p_slug_ad, p_capacity_ad, p_lat_ad, p_lon_ad, p_author_id_ad, 	p_house_type_id_ad)
	
  SELECT LAST_INSERT_ID() AS v_last_ad_id

  INSERT INTO  `image` (ad_id, title, url) VALUES (v_last_ad_id, 	p_title_image, p_url_image)
	
  INSERT INTO `equipment_ad` (equipment_id, ad_id) VALUES (p_equipment_id, 	v_last_ad_id)
	
  INSERT INTO `theme_ad` (theme_id, ad_id) VALUES (p_theme_id, 	v_last_ad_id)

END |

