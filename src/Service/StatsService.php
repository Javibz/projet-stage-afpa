<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class StatsService {

    private $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    public function getStats(){
        $users = $this->getUsersCount();
        $ads = $this->getAdsCount();
        $bookings = $this->getBookingsCount();
        $comments = $this->getCommentsCount();

        return compact('users', 'ads', 'bookings', 'comments');
    }

    /**
     * Traduction de la requête DQL à MySQL
     * SELECT COUNT(*) FROM `user`
     * 
     * Permet d'obtenir le nombre total d'utilisateurs
     *
     */
    public function getUsersCount(){
        // On va utiliser la méthode createQuery() pour créer des requêtes DQL (Doctrine Query Lenguage) personnalisées
        return $this->manager->createQuery('SELECT COUNT(u) FROM App\Entity\User u')->getSingleScalarResult();
    }

    /**
     * Traduction de la requête DQL à MySQL
     * SELECT COUNT(*) FROM `ad`
     * 
     * Permet d'obtenir le nombre total d'annonces
     *
     */
    public function getAdsCount(){

        return $this->manager->createQuery('SELECT COUNT(a) FROM App\Entity\Ad a')->getSingleScalarResult();
    }

    /**
     * Traduction de la requête DQL à MySQL
     * SELECT COUNT(*) FROM `booking`
     * 
     * Permet d'obtenir le nombre total de réservations
     *
     */
    public function getBookingsCount(){

        return $this->manager->createQuery('SELECT COUNT(r) FROM App\Entity\Booking r')->getSingleScalarResult();
    }

    /**
     * Traduction de la requête DQL à MySQL
     * SELECT COUNT(*) FROM `comment`
     * 
     * Permet d'obtenir le nombre total de commentaires
     *
     */
    public function getCommentsCount(){

        return $this->manager->createQuery('SELECT COUNT(c) FROM App\Entity\Comment c')->getSingleScalarResult();
    }

    /**
     * Traduction de la requête DQL à MySQL
     * SELECT AVG(comment.note) as note, ad.title, ad.id, user.first_name, user.last_name, user.avatar
     * FROM comment
     * INNER JOIN ad on ad.id = comment.ad_id
     * INNER JOIN user on user.id = comment.author_id
     * GROUP BY ad.title
     * ORDER BY note DESC
     * LIMIT 5
     * 
     * Permet d'afficher les principales statistiques des annonces, on peut paser en paramettre l'ordre ASC ou DESC
     *
     * @param string $order
     */
    public function getAdsStats($order, $limit) {

        return $this->manager->createQuery(
            'SELECT AVG(c.note) as note, a.title, a.id, u.firstName, u.lastName, u.avatar
            FROM App\Entity\Comment c
            JOIN c.ad a
            JOIN a.author u
            GROUP BY a
            ORDER BY note ' . $order
        )->setMaxResults($limit)
        ->getResult();
    }    
}