<?php

namespace App\Service;

use Twig\Environment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PaginationService {

    private $entityClass;
    private $limit = 10;
    private $currentPage = 1;
    private $manager;
    private $twig;
    private $route;
    private $templatePath;

    public function __construct(EntityManagerInterface $manager, Environment $twig, RequestStack $request, $templatePath) {
        $this->manager = $manager;
        $this->twig = $twig;
        $this->route = $request->getCurrentRequest()->attributes->get('_route');
        $this->templatePath = $templatePath;
    }

    public function setTemplatePath($templatePath) {
        $this->templatePath = $templatePath;
    }

    public function getTemplatePath() {
        return $this->templatePath;
    }

    public function setRoute($route) {
        $this->route = $route;
        return $this;
    }

    public function getRoute() {
        return $this->route;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        return $this;
    }

    public function getPage()
    {
        return $this->currentPage;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Permet de preparer l'affichage html/twig
     *
     */
    public function display() {
        $this->twig->display('admin/partials/pagination.html.twig', [
            'page' => $this->currentPage,
            'pages' => $this->getPages(),
            'route' => $this->route
        ]);
    }

    /**
     * Permet d'obtenir le nombre de pages
     * 
     * @return int $pages
     *
     */
    public function getPages() {
        // On obtient tous les enregistrements d'une table de la BDD
        $repo = $this->manager->getRepository($this->entityClass);
        // On obtient le nombre total de lignes/entrées
        $total = count($repo->findAll());

        // Arroundir le total pour obtenir le nombre de pages. Si total = 22 on aura 3 pages
        $pages = ceil($total / $this->limit);

        return $pages;
    }

    /**
     * Permet de récuperer les données de la BDD
     * 
     * @throws Exception si la proprieté $entityClass n'es pas définie
     * 
     * @return array
     *
     */
    public function getData() {
        // On affiche un message d'erreur pour faciliter la résolution des problèmes 
        if(empty($this->entityClass)) {
            throw new \Exception("Vous n'avez pas spécifié l'entité sur laquelle nous devons paginer ! Utilisez la méthode setEntityClass() de votre objet PasginationService !");
        }

        // On calcule le point de départ
        $offset = $this->currentPage * $this->limit - $this->limit;

        // On demande au repositry de nous trouver les éléments(les données)
        $repo = $this->manager->getRepository($this->entityClass);
        $data = $repo->findBy([], [], $this->limit, $offset);

        // On returne les éléments en question
        return $data;
    }
}