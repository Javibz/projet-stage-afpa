<?php

namespace App\Form;

use App\Entity\Theme;
use App\Entity\User;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends ApplicationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickName', TextType::class, $this->getConfiguration("Pseudonime", "Votre pseudonime..." ))
            ->add('lastName', TextType::class, $this->getConfiguration("Nom", "Votre nom de famille..."))
            ->add('firstName', TextType::class, $this->getConfiguration("Prénom", "Votre prénom..."))
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Votre email ..."))
            ->add('password', PasswordType::class, $this->getConfiguration("Mot de passe", "Votre mot de passe..."))
            ->add('passwordConfirmation', PasswordType::class, $this->getConfiguration('Confirmer le mot de passe', "Veuillez confirmer votre mot de pasee"))
            ->add('phone', TextType::class, $this->getConfiguration("Téléphone", "Votre numéro de contacte..."))
            ->add('address', TextType::class, $this->getConfiguration("Adresse", "Nom et numéro de votre rue"))
            ->add('zipCode', TextType::class, $this->getConfiguration("Code Postal", "Code postal de votre ville"))
            ->add('city', TextType::class, $this->getConfiguration("Ville", "Nom de votre ville"))
            ->add('country', TextType::class, $this->getConfiguration("Pays", "Nom du pays"))
            ->add('birthDate', BirthdayType::class, $this->getConfiguration("Votre date de naissance", "Votre date de naissance"))
            ->add('description', TextareaType::class, $this->getConfiguration("Description", "Présentez-vous à la communauté et parlez-nous de vous pour mieux vous connaitre !"))
            ->add('avatar', UrlType::class, $this->getConfiguration("URL de votre avatar", "Url de l'image de votre avatar..."))
            ->add(
                'themes',
                EntityType::class,
                [
                    'class' => Theme::class,
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
