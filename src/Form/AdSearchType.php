<?php

namespace App\Form;

use App\Entity\Equipment;
use App\Entity\Theme;
use App\Entity\HouseType;
use App\Entity\AdSearch;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class AdSearchType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, $this->getConfiguration(false, 'Ville', ['required' => false]))
            ->add('country', TextType::class, $this->getConfiguration(false, 'Pays', ['required' => false]))
            ->add('rooms', IntegerType::class, $this->getConfiguration(false, 'Nombre de chambres', ['required' => false]))
            ->add('beds', IntegerType::class, $this->getConfiguration(false, 'Nombre de lits', ['required' => false]))
            ->add('bathrooms', IntegerType::class, $this->getConfiguration(false, 'Nombre de salles de bain', ['required' => false]))
            ->add(
                'themes',
                EntityType::class,
                [
                    'class' => Theme::class,
                    'required' => false,
                    'choice_label' => 'name',
                    'expanded' => false,
                    'multiple' => true,
                ]
            )
            ->add(
                'houseTypes',
                EntityType::class,
                [
                    'class' => HouseType::class,
                    'required' => false,
                    'choice_label' => 'name',
                    'expanded' => false,
                    'multiple' => true,
                ]
            )
            ->add(
                'equipments',
                EntityType::class,
                [
                    'class' => Equipment::class,
                    'required' => false,
                    'choice_label' => 'name',
                    'expanded' => false,
                    'multiple' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdSearch::class,
            'method' => 'get',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
