<?php

namespace App\Form;

use App\Entity\Ad;
use App\Entity\Theme;
use App\Form\ImageType;
use App\Entity\Equipment;
use App\Entity\HouseType;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AnnonceType extends ApplicationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration("Titre", "Donnez un titre à votre annonce"))
            ->add('description', TextareaType::class, $this->getConfiguration("Description", "Ecrivez les détails de votre annonce, decrivez votre bien et faites en sorte de donner envie de louer votre bien !"))
            ->add('address', TextType::class, $this->getConfiguration("Adresse", "Nom et numéro de votre rue"))
            ->add('zipCode', TextType::class, $this->getConfiguration("Code Postal", "Code postal de votre ville"))
            ->add('city', TextType::class, $this->getConfiguration("Ville", "Nom de votre ville"))
            ->add('country', TextType::class, $this->getConfiguration("Pays", "Nom du pays"))
            ->add('rooms', IntegerType::class, $this->getConfiguration("Nombre de Chambres", "Combien de chambres a le bien à louer"))
            ->add('beds', IntegerType::class, $this->getConfiguration("Nombre de lits", "Nombre de lits totals"))
            ->add('bathrooms', IntegerType::class, $this->getConfiguration("Nombre de salles de bain", "Nombre de salles de bains"))
            ->add('capacity', IntegerType::class, $this->getConfiguration("Capacité du logement", "Capacité du logement"))
            ->add('price', IntegerType::class, $this->getConfiguration("Prix par nuit", "Le prix par nuité"))
            ->add('coverImage', FileType::class, $this->getConfiguration("URL de l'image principale", "uploadez une image", ['data_class'=> null]))
            ->add('houseType', EntityType::class,
                            [
                                'class' => HouseType::class,
                                'choice_label' => 'name',
                                'expanded' => true,
                                'multiple' => false,
                            ]
            )
            ->add('equipments', EntityType::class,
                            [
                                'class' => Equipment::class,
                                'choice_label' => 'name',
                                'expanded' => true,
                                'multiple' => true,
                            ],
            ['required' => true]
            )
            ->add('themes', EntityType::class,
                            [
                                'class' => Theme::class,
                                'choice_label' => 'name',
                                'expanded' => true,
                                'multiple' => true,
                            ],
            ['required' => true]
            )
            ->add('images', CollectionType::class,
                            [
                                'entry_type' => ImageType::class,
                                'allow_add' => true,
                                'allow_delete' => true
                            ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
