<?php

namespace App\Form;

use App\Entity\Booking;
use App\Form\ApplicationType;
use App\Form\DataTransformer\FrenchToDateTimeTransform;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BookingType extends ApplicationType
{

    private $transformer;
    public function __construct(FrenchToDateTimeTransform $transformer) {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('arriveDate', TextType::class, $this->getConfiguration("Date d'arrivée", "Date à laquelle vous arrivez"))
            ->add('leaveDate', TextType::class, $this->getConfiguration("Date de sortie", "Date à laquelle vous quitez la location"))
            ->add('travelers', IntegerType::class, $this->getConfiguration("Voyageurs", "Nombre de voygeurs"))
            ->add('comment', TextareaType::class, $this->getConfiguration(false, "Laissez un commentaire au proprietaire si vous en avez besoin.", ["required" => false]))
        ;

        $builder->get('arriveDate')->addModelTransformer($this->transformer);
        $builder->get('leaveDate')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'validation_groups' => ['Default', 'front']
        ]);
    }
}
