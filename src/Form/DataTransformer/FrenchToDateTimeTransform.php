<?php
namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FrenchToDateTimeTransform implements DataTransformerInterface
{
    public function transform($date) {
        if ($date === null) {
            return '';
        }

        return $date->format('d/m/Y');
    }

    public function reverseTransform($frenchDate) {
        if($frenchDate === null) {
            // Si la date est null on renvoie une exception
            throw new TransformationFailedException('Vous devez saisir une date');
        }

        $date = \DateTime::createFromFormat('d/m/Y', $frenchDate);

        if($date === null) {
            // Si la date est null on renvoie une exception
            throw new TransformationFailedException("Le format de la date n'est pas corecte");
        }

        return $date;
    }
}