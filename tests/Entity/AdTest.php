<?php
namespace App\Tests\Entity;

use App\Entity\Ad;
use ReflectionClass;
use PHPUnit\Framework\TestCase;

class AdTest extends TestCase
{
    public function testGetFullAddressMethod(){

      $ad = new Ad();
      // On utilise la classe Reflection pour changer
      // la visibilité des attributs qui sont privés.
      $reflector = new ReflectionClass(Ad::class);

      // On rend les différentes proprietés visibles
      $address = $reflector->getProperty('address');
      $address->setAccessible(true);

      $zipCode = $reflector->getProperty('zipCode');
      $zipCode->setAccessible(true);

      $city = $reflector->getProperty('city');
      $city->setAccessible(true);

      $country = $reflector->getProperty('country');
      $country->setAccessible(true);

      // On assigne des valeurs aux propriétés
      $addressVal = $address->setValue($ad, "3 rue de l'Aiglon");
      $zipCodeVal = $zipCode->setValue($ad, '34090');
      $cityVal = $city->setValue($ad, 'Montpellier');
      $countryVal = $country->setValue($ad, 'France');

      // On teste si on obtient le résultat esperé
      $this->assertEquals("3 rue de l'Aiglon, 34090 Montpellier, France", $ad->getFullAddress("$addressVal, $zipCodeVal $cityVal, $countryVal"));
    }
}