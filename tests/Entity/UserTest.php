<?php
namespace App\Tests\Entity;

use App\Entity\User;
use ReflectionClass;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGetFullNameMethod(){

      $user = new User();
      // On utilise la classe Reflection pour changer la visibilité des attributs qui sont privés.
      $reflector = new ReflectionClass(User::class);

      // On rend la proprietés visibles
      $firstName = $reflector->getProperty('firstName');
      $firstName->setAccessible(true);

      $lastName = $reflector->getProperty('lastName');
      $lastName->setAccessible(true);

      // On assigne des valeurs aux propriétés
      $firstNameVal = $firstName->setValue($user, 'Javi');
      $lastNameVal = $lastName->setValue($user, 'Ramon');

      $this->assertEquals("Javi Ramon", $user->getFullName("$firstNameVal $lastNameVal"));
    }
}
