<?php
namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
  public function testHomepagePage() {
    $client = static::createClient();

    $client->request('GET', '/home');
    $this->assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testH1HomepagePage() {
    $client = static::createClient();

    $client->request('GET', '/home');

    $this->assertSelectorTextContains('h1', 'Bonjour les voyageurs');
  }
}
