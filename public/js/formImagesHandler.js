  $(document).ready(function() {

    // On affiche le nom de fichier à la place du placeholder
    $('#annonce_coverImage').change(function() {
      let filePath = $('#annonce_coverImage').val()
      $('.custom-file label')[0].innerText = filePath.replace(/C:\\fakepath\\/, "");
    })
    
    // On affiche le nom des fichiers à la place des placeholders du sous formulaire des images
    // $('[id ^= block_annonce_images_]').change(function() {
    //   let filePathImages = $('[id^=annonce_images_][id$=_url]').val();
    //   let fileInputImages = $('[id ^= block_annonce_images_] .custom-file-label')[0].innerText;
    //   $('[id ^= block_annonce_images_] .custom-file-label')[0].innerText = filePathImages.replace(/C:\\fakepath\\/, "");
    // })

      $('#ajout-image').click(function() {
        // On récupére la valeur du input hidden qui va nous donner le nombre de formulaires. De base on prdéfinie la valeur à 0
        const index = parseInt($('#widget-counter').val());
    
        // On récupére le data-prototype(le formulaire) et on remplace le mot '__name__' par la valeur du compteur 'index'.
        // de cette façon on aura : annonce_images_1 à la place de annonce_images___name__
        const formTemplate = $('#annonce_images').data('prototype').replace(/__name__/g, index);
    
        // On ajoute le code du prototype pour créer un nouveau formulaire
        $('#annonce_images').append(formTemplate);
    
        // On incremente de 1 la valeur du compteur
        $('#widget-counter').val(index + 1);
        
        handleDeleteButton();
      });

      function handleDeleteButton() {
        $('button[data-action="delete"]').click(function(){
          const target = this.dataset.target;
          $(target).remove();
        });

      }

      function updateCounter() {
        const count = parseInt($('#ajout-image div.form-group').length);

        $('#widget-counter').val(count);
      }

      updateCounter();
      handleDeleteButton();
});