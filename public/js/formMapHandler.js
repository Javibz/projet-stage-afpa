$(document).ready(function () {

    // On initalise la carte
    var mymap = L.map('formMap').setView([43.6112422, 3.8767337], 13);

    // On determine quel affichage on veut pour notre carte
    L.tileLayer('//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: 'donn&eacute;es &copy;<a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - Tiles courtesy of<a href="https://ot.openstreetmap.org/">Humanitarian OpenStreetMap Team</a>',
        minZoom: 1,
        maxZoom: 19
    }).addTo(mymap);

    let datas = $('.ad-coords');
    // on declare le marqueur pour l'utiliser par la suite
    let marker;
    // si on est dans la page show on va placer le marker sur la carte
    if(datas.length) {
        // on positione le marqueur avec les coordonées actuelles 
        marker = new L.marker([datas.data('lat'), datas.data('lon')]).addTo(mymap);
    }

    // Quand on clique sur la carte on récupére la lattitude et longitude du point cliqué et on récupére les données de l'adresse avec l'appel API sur Nominatim
    mymap.on('click', function (ev) {
        // ev is an event object (MouseEvent in this case)
        let coords = ev.latlng

        // Create a request variable and assign a new XMLHttpRequest object to it.
        var request = new XMLHttpRequest()

        // Open a new connection, using the GET request on the URL endpoint
        // On ajoute la lat et lon récupré precedement dans l'url et on l'envoie avec la requette GET vers l'url ciblé au format geojson (ce format on peut le changer si on veut --> voir la doc Nominatim) 
        request.open('GET', 'https://nominatim.openstreetmap.org/reverse?format=geojson&lat=' + coords['lat'] + '&lon=' + coords['lng'] + '&accept-language=fr-FR', true)

        // On verifie s'il existe déjà un marquer sur la carte, s'il existe un, on l'elimine et on crée un nouveau
        if (marker != undefined) {
            mymap.removeLayer(marker);
        }

        // on crée le nouveau marqueur avec la nouvelle position
        marker = new L.marker([coords['lat'], coords['lng']]).addTo(mymap);

        // On récupére la response et on manipule les données
        request.onload = function () {
            // On accéde aux donées JSON transforme la response qu'on reçoit en JSON en array
            var data = JSON.parse(this.response)

            
            // on récupére le numéro de maison
            let houseNumber = data['features'][0]['properties']['address']['house_number'];

            // on récupére le nom de la rue
            let road = data['features'][0]['properties']['address']['road'];

            // on récupére le nom de la rue
            let pedestrian = data['features'][0]['properties']['address']['pedestrian'];

            // on récupére le code postal
            let postCode = data['features'][0]['properties']['address']['postcode'];

            // on récupére le nom de la ville
            let city = data['features'][0]['properties']['address']['city'];

            // on récupére le nom du village
            let village = data['features'][0]['properties']['address']['village'];

            // on récupére le nom de la comune
            let town = data['features'][0]['properties']['address']['town'];

            // on récupére le nom du pays
            let country = data['features'][0]['properties']['address']['country'];

            // console.log(data['features'][0]['properties']['address']);
            console.log(data);

            // On va afficher sur le formulaire les informations qu'on a récupéré à chaque click
            // S'il n'y  pas de numéro de maison, on affiche seulement la nom de la rue
            if (houseNumber === undefined || houseNumber === undefined) {
                if (road === undefined) {
                    $('#annonce_address').val(pedestrian);
                } else {
                    $('#annonce_address').val(road);
                }

            } else if (road === undefined) {
                $('#annonce_address').val(houseNumber + " " + pedestrian);

            } else {
                $('#annonce_address').val(houseNumber + " " + road);
            }

            // On affiche le code postal
            $('#annonce_zipCode').val(postCode);

            // Si le nom de ville n'est pas definie on affiche le nom de village (voir s'il y a d'autres types et ajouter à la condition)
            if (city === undefined && village === undefined) {
                $('#annonce_city').val(town)

            } else if (village === undefined && town === undefined) {
                $('#annonce_city').val(city)
            } else {
                $('#annonce_city').val(village)
            }

            // On affiche le nom du pays
            $('#annonce_country').val(country);
        }
        // Send request
        request.send()
    });
});