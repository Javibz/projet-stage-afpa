$(document).ready(function () {
    // On customise les menus select du moteur de recherche par filtres
    $('select').select2();
    // quand la page index des annonces est chargé on cache le formulaire de recherche
    $('.search-form').hide();
    
    // Quand on clique sur le bouton filtrer on affiche le formulaire
    $('.filter-btn').click(function () {
        $('.search-form').toggle("slow");
    });

    // On initalise la carte
    var mymap = L.map('mapid').setView([43.6112422, 3.8767337], 13);

    // On determine quel affichage on veut pour notre carte
    L.tileLayer('//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: 'donn&eacute;es &copy;<a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - Tiles courtesy of<a href="https://ot.openstreetmap.org/">Humanitarian OpenStreetMap Team</a>',
        minZoom: 1,
        maxZoom: 19
    }).addTo(mymap);
    
    // On cible les coordonées de chaque annonce
    let coords = $('.js-coords');

    // On crée un tableau avec les coordonées de chaque annonce en ciblant les attributs data-lat et data-lon
    let coordsMarkers = [];
    
    for (let i = 1; i <= coords.length; i++) {
        let datas = $('.d' + i + ' span')
        coordsMarkers[i - 1] = [datas.data('lat'), datas.data('lon')];
    };

    // On crée un marqueur sur la carte pour chaque annonce
    let coverImageUrl = $('.card-img-top').attr('src');
    let titre = $('.card-title');
    let price = $('.price-card');

    for (i = 0; i < coordsMarkers.length; i++) {
        // on crée le marqueur et on lui ajoute du html via la méthode bindPopup, quand on clique sur un marqueur on peut voir une popup avec des infos de l'annonce.
        new L.marker([coordsMarkers[i][0], coordsMarkers[i][1]]).bindPopup(
            '<img src="' + coverImageUrl + '" width="250px"><p>' + titre[i].innerText + '</p><p><strong>' + price[i].innerText +'</strong></p>'
            ).addTo(mymap);
    }
});
